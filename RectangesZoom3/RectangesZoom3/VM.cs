﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CanvasMap;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using SharpKml.Dom;
using SharpKml.Engine;
using Location = CanvasMap.Location;

namespace RectangesZoom3
{
    public class VM:ViewModelBase
    {
       public ICommand SaveRegionsCommand { get; private set; }

       public VM()
       {
           SaveRegionsCommand = new AskSaveRelayCommand<Func<IList<GeoPolygon>>>(SaveRegions,"kml"); //RelayCommand<Func<IList<GeoPolygon>>>(SaveRegions);
           AddRegionsCommand = new AskOpenRelayCommand<Action<IList<GeoPolygon>>>(AddRegions,"kml"); //new RelayCommand<Action<IList<GeoPolygon>>>(AddRegions);
       }

       public ICommand AddRegionsCommand { get; set; }

       private void AddRegions(Action<IList<GeoPolygon>> setter,string fileName)
       {
           var saveFileName = fileName;
           List<GeoPolygon> polys = new List<GeoPolygon>();
           using (FileStream stream = File.OpenRead(saveFileName))
           {
               KmlFile kml = KmlFile.Load(stream);
             var kmlTag=  kml.Root as Kml;
             
               var doc =kmlTag.Feature as Document;
               //var folder = doc.Features.OfType<Folder>().Single();
               var filepolys = doc.Features.OfType<Placemark>();
               foreach (var fp in filepolys)
               {
                   var p = fp.Geometry as Polygon;
                   var c = p.OuterBoundary.LinearRing.Coordinates;
                   GeoPolygon gp = new GeoPolygon();
                   foreach (var coord in c)
                   {
                       gp.AddLast(new Location(coord.Latitude, coord.Longitude));
                   }
                  // gp.RemoveLast();
                   polys.Add(gp);
               }
           }
        
           setter(polys);


       }

       private void SaveRegions(Func<IList<GeoPolygon>> getter,string fileName)
       {
           var polys = getter();
           var saveFileName = fileName;

        var kmltag = new Kml();
          
           var doc = new Document();
           kmltag.Feature = doc;
        
           var name = -1;
           name++;
        
           foreach (var poly in polys)
           {
               var pl = new Placemark();
               var p = new Polygon();
               pl.Name = name.ToString();
               pl.Geometry = p;
               var ob = new OuterBoundary();
               p.OuterBoundary = ob;
               var lr = new LinearRing();
               lr.Tessellate = true;
               ob.LinearRing = lr;
               lr.Coordinates = new CoordinateCollection();
               foreach (var item in poly)
               {
                   lr.Coordinates.Add(new SharpKml.Base.Vector(item.Latitude, item.Longitude,0));
               }
               doc.AddFeature(pl);
           }
             KmlFile kml = KmlFile.Create(kmltag, false);
           File.Delete(saveFileName);
           using (FileStream stream = File.OpenWrite(saveFileName))
           {
               kml.Save(stream);
           }

       }
    }
}
