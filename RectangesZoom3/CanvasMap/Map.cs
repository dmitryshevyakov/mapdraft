﻿using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;

namespace CanvasMap
{
    public class Map : MapMiddle
    {
        static Map()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Map),
                new FrameworkPropertyMetadata(typeof(Map)));
        }
        public static readonly DependencyProperty MouseProperty = DependencyProperty.Register(
            "Mouse", typeof(Location), typeof(Map), new PropertyMetadata(default(Location)));

        public Map()
        {
            StartRegionCommand = new SimpleCommand(StartRegion);
            EndRegionCommand = new SimpleCommand(EndRegion);
        }

        public ICommand StartRegionCommand { get; set; }
        public ICommand EndRegionCommand { get; set; }

        public Location Mouse
        {
            get { return (Location)GetValue(MouseProperty); }
            set { SetValue(MouseProperty, value); }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            Mouse = MouseCoords();
        }
    }
}