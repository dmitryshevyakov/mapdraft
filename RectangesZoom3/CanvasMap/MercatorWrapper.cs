using System;
using System.Diagnostics;
using System.Windows;

namespace CanvasMap
{
    public static class MercatorWrapper
    {
        private const double maxY = 34619289.3718563;
        private const double maxX = 20037509;

        //180>=y>=-180
        public static double YToLatitude(double y)
        {
            Debug.Print(y.ToString());
            var res = Math.Atan(Math.Sinh(y * Math.PI / 180d)) / Math.PI * 180d;
            return res;
        }

        public static double LatitudeToY(double latitude)
        {
            if (latitude <= -90d)
            {
                return double.NegativeInfinity;
            }

            if (latitude >= 90d)
            {
                return double.PositiveInfinity;
            }

            latitude *= Math.PI / 180d;
            return Math.Log(Math.Tan(latitude) + 1d / Math.Cos(latitude)) / Math.PI * 180d;
        }

        public static Point GetPoint(Location p, Rect viewPort, byte zoom)
        {
            var vh = Constants.TileSize * Math.Pow(2, zoom);
            var x = MercatorOSM.lonToX(p.Longitude * -1);
            var percentX = (x - maxX) / (-2 * maxX);
            var point = new Point();
            point.X = percentX * vh + viewPort.X;
            var y = LatitudeToY(p.Latitude * -1);
            var percentY = (y + 180d) / 360d;
            point.Y = percentY * vh + viewPort.Y;

            return point;
        }

        public static Location GetLocation(Point p, Rect viewPort, byte zoom)
        {
            var vh = Constants.TileSize * Math.Pow(2, zoom);
            var percentincurrent = (p.Y - viewPort.Y) / vh;
            var y1 = percentincurrent * 360d - 180d;
            var lat2 = -1 * YToLatitude(y1);
            var percentX = (p.X - viewPort.X) / vh;
            var x = maxX - 2 * maxX * percentX;
            var lon = -1 * MercatorOSM.xToLon(x);
            return new Location(lat2, lon);
        }
    }
}