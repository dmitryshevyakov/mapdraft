using System;
using System.Windows.Input;

namespace CanvasMap
{

    internal class AddPolygonsCommand:ICommand
    {
        public bool CanExecute(object parameter)
        {
            throw new NotImplementedException();
        }

        public void Execute(object parameter)
        {
            throw new NotImplementedException();
        }

        public event EventHandler CanExecuteChanged;
    }


    internal class SimpleCommand : ICommand
    {
        private readonly Func<bool> _canexecute;
        private readonly Action _execute;

        public SimpleCommand(Action execute, Func<bool> canexecute)
        {
            _execute = execute;
            _canexecute = canexecute;
        }

        public SimpleCommand(Action execute)
            : this(execute, () => true)
        {
        }

        public bool CanExecute(object parameter)
        {
            return _canexecute();
        }

        public void Execute(object parameter)
        {
            _execute();
        }

        public event EventHandler CanExecuteChanged;

        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
                CanExecuteChanged(this, new EventArgs());
        }
    }
}