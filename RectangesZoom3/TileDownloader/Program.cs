﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TileDownloader
{
    class Program
    {
        private const string urlTemplate = @"http://tile.openstreetmap.org/{0}/{1}/{2}.png";

        static void Main(string[] args)
        {
            CacheFolder = Path.Combine(@"\\rusfinance.ru\files\SamaraFiles2\LSUGenesis", "MapCache");
            double num = 0;
            for (byte z = 0; z < 18; z++)
            {
                var max = Math.Pow(2, z);
                num += Math.Pow(max, 2);
            }

            var tf = new TaskFactory();
            List<Task> l = new List<Task>();
            for (byte z = 0; z < 18; z++)
            {
                var z1 = z;
                Action act = () => DownLoad(z1);
                var t = tf.StartNew(act);
                l.Add(t);

            }


            Task.WaitAll(l.ToArray());

            Console.ReadKey();
        }
        private static void DownloadZplus()
        {
            for (byte z = 0; z <= 18; z++)
            {
                DownLoad(z);
            }
        }
        private static void DownloadZminus()
        {
            for (byte z = 18; z >= 0; z--)
            {
                DownLoad(z);
            }
        }

        private static void DownLoad(byte z)
        {
            var max = Math.Pow(2, z) - 1;
            var list = Enumerable.Range(0, (int)max).ToList();
            list.Shuffle();
            Queue<int> q = new Queue<int>(list);
            while (q.Any())
            {
                var x = q.Dequeue();
                for (int y = 0; y <= max; y++)
                {
                    GetImage(z, x, y);
                }
            }


        }


        private static string CacheFolder;
        private static void GetImage(byte zoom, int x, int y)
        {

            var cachename = Path.Combine(CacheFolder, zoom.ToString(), x.ToString(), y + ".png");
            var folder = Path.Combine(CacheFolder, zoom.ToString(), x.ToString());
            var url = string.Format(urlTemplate, zoom, x, y);
            if (!File.Exists(cachename))
            {
                WebClient wc = new WebClient();


                wc.Proxy = WebRequest.DefaultWebProxy;
                wc.Proxy.Credentials = CredentialCache.DefaultNetworkCredentials;
                try
                {
                    Directory.CreateDirectory(folder);
                    wc.DownloadFile(url, cachename);
                    Console.WriteLine("download z:{0} x:{1} y:{2}", zoom, x, y);
                }
                catch (WebException)
                {

                }

            }

        }
    }

    static class Ext
    {
        private static Random rng;
        static Ext()
        {
            rng = new Random(DateTime.Now.Millisecond);
        }
        public static void Shuffle<T>(this IList<T> list)
        {

            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
